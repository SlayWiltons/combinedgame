﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Linq;
using System;

public class AddPanelController : FunctionalController
{
    public GameObject AddPanel;
    public InputField objDeviceName;
    public InputField objDeviceID;
    public Dropdown objDeviceType;
    public Dropdown objDevicesList;
    public string[] strExepMessages = { "Необходимо заполнить все поля", "Устройство с таким именем уже существует", "Устройство с таким идентификатором уже существует" };
    private List<string> DeviceParamsList;

    public void AddNewIdDevice()
    {
        bool[] bExepCheck = { false, false, false };
        string checkName = objDeviceName.text.Replace(" ", string.Empty);
        string checkID = objDeviceID.text.Replace(" ", string.Empty);
        if (checkName == "" || checkID == "")
        {
            bExepCheck[0] = true;
        }
        if (new FileInfo(strPathFileID).Length != 0)
        {            
            for (int i = 0; i < DevicesList.Count; i++)
            {
                DeviceParamsList = DevicesList[i].Split(';').ToList();
                if (DeviceParamsList[0] == objDeviceName.text.Trim())
                {
                    bExepCheck[1] = true;
                    break;
                }
                if (DeviceParamsList[2] == objDeviceID.text.Trim())
                {
                    bExepCheck[2] = true;
                    break;
                }
            }
        }
        for (int j = 0; j < bExepCheck.Length; j++)
        {
            if (bExepCheck[j] == true)
            {
                DialogWindowsController.ShowWarningWindow(strExepMessages[j]);
                break;
            }
        }
        if (bExepCheck[0] == false && bExepCheck[1] == false && bExepCheck[2] == false)
        {
            AddingNewDevice();
        }
    }

    public void ClosePanel()
    {
        objDeviceID.text = "";
        objDeviceName.text = "";
        AddPanel.SetActive(false);
    }

    void AddingNewDevice()
    {
        DevicesList.Add(objDeviceName.text.Trim() + ';' + objDeviceType.options[objDeviceType.value].text + ';' + objDeviceID.text.Trim());
        File.WriteAllLines(strPathFileID, DevicesList);
        DevicesListUpdate(objDevicesList);
        objDevicesList.value = objDevicesList.options.Count - 1;// 
        DialogWindowsController.ShowWarningWindow("Устройство добавлено");
        ClosePanel();
    }
}