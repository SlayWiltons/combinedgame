﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using UnityEngine.UI;

public class BikerSpeed : MonoBehaviour
{
    float speed;
    float distanceTravelled;

    void Update()
    {
        if (StartController.neededType == "Вело")
        {
            speed = GetComponent<Follower_Biker>().speed / 10;
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 1.5f;
            transform.position = GetComponent<Follower_Biker>().pathCreator.path.GetPointAtDistance(distanceTravelled);
            transform.rotation = GetComponent<Follower_Biker>().pathCreator.path.GetRotationAtDistance(distanceTravelled);
        }
        if (StartController.neededType == "Кинематика")
        {
            speed = GetComponent<Follower_Kinemat>().speed / 10;
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 1.5f;
            transform.position = GetComponent<Follower_Kinemat>().pathCreator.path.GetPointAtDistance(distanceTravelled);
            transform.rotation = GetComponent<Follower_Kinemat>().pathCreator.path.GetRotationAtDistance(distanceTravelled);
        }
        if (StartController.neededType == "Дорожка")
        {
            speed = GetComponent<Follower_Runner>().speed;
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 1.5f;
            transform.position = GetComponent<Follower_Runner>().pathCreator.path.GetPointAtDistance(distanceTravelled);
            transform.rotation = GetComponent<Follower_Runner>().pathCreator.path.GetRotationAtDistance(distanceTravelled);
        }
    }
}
