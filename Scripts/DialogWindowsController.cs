﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogWindowsController : MonoBehaviour
{
    public static GameObject DialogQWindow;
    public static GameObject DialogInfoWindow;
    public static GameObject Ok_ExitButton;
    public static GameObject Ok_DeleteButton;
    public static GameObject WarningText;
    public static GameObject QuestionText;

    private void Awake()
    {
        DialogQWindow = GameObject.Find("Canvas_QDialog");
        DialogInfoWindow = GameObject.Find("Canvas_DWindow");
        QuestionText = GameObject.Find("Text_QDialog");
        WarningText = GameObject.Find("Text_Warning");
        Ok_ExitButton = GameObject.Find("Button_Ok_Exit");
        Ok_DeleteButton = GameObject.Find("Button_Ok_Delete");
    }

    void Start()
    {
        CancelWindow();
    }

    public static void ShowQuestionWindow(string question, byte IDWindow)
    {
        DialogQWindow.SetActive(true);
        QuestionText.GetComponent<Text>().text = question;
        if (IDWindow == 1)
        {
            Ok_ExitButton.SetActive(true);
            Ok_DeleteButton.SetActive(false);
        }
        if (IDWindow == 2)
        {
            Ok_DeleteButton.SetActive(true);
            Ok_ExitButton.SetActive(false);
        }
    }

    public static void ShowWarningWindow(string warning)
    {
        DialogInfoWindow.SetActive(true);
        WarningText.GetComponent<Text>().text = warning;
    }

    public void CancelWindow()
    {
        if (DialogQWindow.activeSelf == true)
        {
            DialogQWindow.SetActive(false);
        }
        if (DialogInfoWindow.activeSelf == true)
        {
            DialogInfoWindow.SetActive(false);
        }
    }
}