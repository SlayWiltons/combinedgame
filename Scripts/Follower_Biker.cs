﻿using UnityEngine;
using PathCreation;
using UnityEngine.UI;
using System;

public class Follower_Biker : MonoBehaviour
{
    public PathCreator pathCreator;
    public float speed;
    private float speedForInfo;
    float distanceTravelled;
    private string info;
    public GameObject textBox;
    private mqttTest_Biker mqttdata;
    public Camera char_camera;

    void Start()
    {
        mqttdata = gameObject.GetComponent<mqttTest_Biker>();
    }

    void Update()
    {
        speed = mqttdata.q / 10;
        if (mqttdata.q < 0)
        {
            mqttdata.q = 0;
        }
        if (mqttdata.q > 100)
        {
            mqttdata.q = 100;
        }
        speedForInfo = speed;
        info = mqttdata.q + " об/мин";
        textBox.GetComponent<Text>().text = info;
        if (MenuController.neededCharacter == "Biker")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 1.5f;
        }
        if (MenuController.neededCharacter == "Runner")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime;
        }
        if (MenuController.neededCharacter == "Car")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 3f;
        }
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);
    }
}