﻿using UnityEngine;
using PathCreation;
using UnityEngine.UI;
using System;

public class Follower_Kinemat : MonoBehaviour
{
    public PathCreator pathCreator;
    public float speed;
    private float speedForInfo;
    float distanceTravelled;
    private string info;
    public GameObject textBox;
    private mqttTest_Kinemat mqttdata;
    public Camera char_camera;

    void Start()
    {
        mqttdata = gameObject.GetComponent<mqttTest_Kinemat>();
    }

    void Update()
    {
        speed = mqttdata.qs / 2;
        if (mqttdata.qs < 0)
        {
            mqttdata.qs = 0;
        }
        if (mqttdata.qs > 20)
        {
            mqttdata.qs = 20;
        }
        speedForInfo = speed;
        info = mqttdata.qs + " об/мин";
        textBox.GetComponent<Text>().text = info;
        if (MenuController.neededCharacter == "Biker")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 1.5f;
        }
        if (MenuController.neededCharacter == "Runner")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime;
        }
        if (MenuController.neededCharacter == "Car")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 3f;
        }
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);
    }
}