﻿using UnityEngine;
using PathCreation;
using UnityEngine.UI;
using System;

public class Follower_Runner : MonoBehaviour
{
    public PathCreator pathCreator;
    public float speed;
    private float speedForInfo;
    float distanceTravelled;
    private string info;
    public GameObject textBox;
    private mqttTest_Runner mqttdata;
    public Camera char_camera;

    void Start()
    {
        mqttdata = gameObject.GetComponent<mqttTest_Runner>();
    }

    void Update()
    {
        speed = mqttdata.qs / 10;
        if (mqttdata.qs < 0)
        {
            mqttdata.qs = 0;
        }
        if (mqttdata.qs > 100)
        {
            mqttdata.qs = 100;
        }
        speedForInfo = speed;
        info = speedForInfo + " км/ч";
        textBox.GetComponent<Text>().text = info;
        if (MenuController.neededCharacter == "Biker")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 1.5f;
        }
        if (MenuController.neededCharacter == "Runner")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime;
        }
        if (MenuController.neededCharacter == "Car")
        {
            distanceTravelled += (speed / 3.6f) * Time.deltaTime * 3f;
        }
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);    
    }
}