﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootStep : MonoBehaviour
{
    [SerializeField] private AudioClip[] footsteps;
    private AudioSource audiosource;

    private void Awake()
    {
        audiosource = GetComponent<AudioSource>();
    }

    private void Step()
    {
        AudioClip clip = GetRandomClip();
        audiosource.PlayOneShot(clip);
    }

    private AudioClip GetRandomClip()
    {
        return footsteps[UnityEngine.Random.Range(0, footsteps.Length)];
    }
}
