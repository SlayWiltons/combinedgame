﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FunctionalController : MonoBehaviour
{
    public static string strPathFileIP;
    public static string strPathFileID;
    public static List<string> DevicesList;

    private void Awake()
    {
        strPathFileIP = Application.dataPath + "/StreamingAssets/ip.txt";
        strPathFileID = Application.dataPath + "/StreamingAssets/id.txt";
    }

    public static void DevicesListUpdate(Dropdown objDevicesList)
    {
        DevicesList = File.ReadAllLines(strPathFileID).ToList();
        objDevicesList.options.Clear();
        for (int i = 0; i < DevicesList.Count; i++) 
        {
            objDevicesList.options.Add(new Dropdown.OptionData(DevicesList[i].Substring(0, DevicesList[i].IndexOf(';'))));
        }
        objDevicesList.RefreshShownValue();
    }

    public static void ChangeMenuElements(GameObject prevMenuElem, GameObject nexMenuElem)
    {
        prevMenuElem.SetActive(false);
        nexMenuElem.SetActive(true);
    }
}