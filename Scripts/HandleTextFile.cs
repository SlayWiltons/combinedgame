﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class HandleTextFile : MonoBehaviour
{
    public string path;
    public string path2;
    public string strIP;
    public string strID;

    public void ReadString()
    {
        
        path = Application.dataPath + "/StreamingAssets/ip.txt";
        path2 = Application.dataPath + "/StreamingAssets/id.txt";
        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        strIP = reader.ReadLine();
        //Debug.Log(reader.ReadToEnd());
        Debug.Log("StrIP: " + strIP);
        reader.Close();
        StreamReader reader2 = new StreamReader(path2);
        strID = reader2.ReadLine();
        //Debug.Log(reader2.ReadToEnd());
        Debug.Log("StrID: " + strID);
        reader2.Close();
    }

    void Start()
    {
        ReadString();
    }
}
