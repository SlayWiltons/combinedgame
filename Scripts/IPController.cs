﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public class IPController : FunctionalController
{
    public InputField IPInputField;
    public Button UpdateIPButton;
    private string IpStr;   //строка ip для проверки изменения адреса

    void Start()
    {
        StreamReader reader = new StreamReader(strPathFileIP);
        IpStr = reader.ReadLine();
        reader.Close();
        IPInputField.text = IpStr;
    }

    public void ButtonIPUpdatePress()
    {
        File.WriteAllText(strPathFileIP, string.Empty);
        StreamWriter writer = new StreamWriter(strPathFileIP);
        writer.WriteLine(IPInputField.text);
        writer.Close();
        IpStr = IPInputField.text.Trim();
        IPValueChanging();
    }

    public void IPValueChanging()   // Наглядно блочим кнопку при совпадении ip или пустой строке
    {
        if (IPInputField.text == IpStr || IPInputField.text == "")
        {
            UpdateIPButton.interactable = false;
        }
        else
        {
            UpdateIPButton.interactable = true;
        }
    }
}
