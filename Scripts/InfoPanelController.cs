﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEditor;
using System.Linq;

public class InfoPanelController : FunctionalController
{
    public Dropdown DevicesListDropdown;
    public InputField DeviceNameField;
    public Dropdown DeviceTypeDropdown;
    public InputField DeviceIdField;
    private string CurrentDevice;
    private List<string> DeviceParamsList;
    private int StringID;
    public GameObject EnableChangesButton;
    public GameObject SaveChangesButton;

    void OnEnable()
    {
        CurrentDevice = DevicesList.FirstOrDefault(s => s.Contains(DevicesListDropdown.options[DevicesListDropdown.value].text));
        StringID = DevicesListDropdown.value;
        DeviceParamsList = CurrentDevice.Split(';').ToList();
        DeviceNameField.text = DeviceParamsList[0];
        for (int i = 0; i < DeviceTypeDropdown.options.Count; i++)
        {
            if (DeviceTypeDropdown.options[i].text == DeviceParamsList[1])
            {
                DeviceTypeDropdown.value = i;
                break;
            }
        }
        DeviceIdField.text = DeviceParamsList[2];
        DeviceNameField.interactable = false;
        DeviceTypeDropdown.interactable = false;
        DeviceIdField.interactable = false;
        ChangeMenuElements(SaveChangesButton, EnableChangesButton);
    }

    public void UpdateDeviceData()
    {
        string checkName = DeviceNameField.text.Replace(" ", string.Empty);
        string checkID = DeviceIdField.text.Replace(" ", string.Empty);
        if (checkName == "" || checkID == "")
        {
            DialogWindowsController.ShowWarningWindow("Необходимо заполнить все поля");
        }
        else
        {
            DevicesList[StringID] = DeviceNameField.text + ';' + DeviceTypeDropdown.options[DeviceTypeDropdown.value].text + ';' + DeviceIdField.text;
            File.WriteAllLines(strPathFileID, DevicesList);
            DevicesListUpdate(DevicesListDropdown);
            gameObject.SetActive(false);
        }
    }

    public void DeleteButtonClick()
    {
        DialogWindowsController.ShowQuestionWindow("Удалить устройство?", 2);        
    }

    public void DeleteDevice()
    {
        DialogWindowsController.DialogQWindow.SetActive(false);
        DevicesList.RemoveAt(StringID);
        File.WriteAllLines(strPathFileID, DevicesList);
        DevicesListUpdate(DevicesListDropdown);
        gameObject.SetActive(false);
    }

    public void EnableChanges()
    {
        ChangeMenuElements(EnableChangesButton, SaveChangesButton);
        DeviceNameField.interactable = true;
        DeviceTypeDropdown.interactable = true;
        DeviceIdField.interactable = true;
    }

    public void ClosePanel()
    {
        gameObject.SetActive(false);
    }
}