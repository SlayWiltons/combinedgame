﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class MenuController : FunctionalController
{
    public Dropdown DevicesListDropdown;
    public GameObject AddingPanel;
    public GameObject InfoPanel;
    public Button ToInfoPanel;
    private string CurrentDevice;
    private List<string> NeededDeviceData;
    public GameObject MenuStage1;
    public GameObject MenuStage2;
    public GameObject MenuStage3_All;
    public GameObject MenuStage3_NoCar;
    public GameObject MenuStage3_CarOnly;
    public Button ToStage2Button;
    public static string NeededType;
    public static string NeededId;

    public GameObject LoadingScreen;
    public Slider slider;
    public Text progressText;

    public static int idScene;
    public static string neededCharacter;
    private GameObject CurrentMenu;

    const string PrefName = "optionvalue";

    void Start()
    {
        DevicesListUpdate(DevicesListDropdown);
        DevicesListDropdown.value = PlayerPrefs.GetInt(PrefName, 0);
    }

    public void OpenAddPanel()
    {
        AddingPanel.SetActive(true);
    }

    public void OpenInfoPanel()
    {
        InfoPanel.SetActive(true);
    }

    public void LoadStadium()
    {
        idScene = 1;
        ToStage3_NoCar();
    }

    public void LoadForest()
    {
        idScene = 2;
        ToStage3_NoCar();
    }

    public void LoadCity1()
    {
        idScene = 3;
        ToStage3_All();
    }

    public void LoadCity2()
    {
        idScene = 4;
        ToStage3_All();
    }

    public void LoadNearCity()
    {
        idScene = 5;
        ToStage3_CarOnly();
    }

    public void LoadWithRunner()
    {
        neededCharacter = "Runner";
        LoadLevel(idScene);
    }

    public void LoadWithBiker()
    {
        neededCharacter = "Biker";
        LoadLevel(idScene);
    }

    public void LoadWithCar()
    {
        neededCharacter = "Car";
        LoadLevel(idScene);
    }

    public void ToStage2()
    {
        SaveData();
        CurrentDevice = DevicesList.FirstOrDefault(s => s.Contains(DevicesListDropdown.options[DevicesListDropdown.value].text));
        NeededDeviceData = CurrentDevice.Split(';').ToList();
        NeededType = NeededDeviceData[1];
        NeededId = NeededDeviceData[2];
        ChangeMenuElements(MenuStage1, MenuStage2);
    }

    public void BackToStage1()
    {
        ChangeMenuElements(MenuStage2, MenuStage1);
    }

    public void ToStage3_All()
    {
        ChangeMenuElements(MenuStage2, MenuStage3_All);
        CurrentMenu = MenuStage3_All;
    }

    public void ToStage3_NoCar()
    {
        ChangeMenuElements(MenuStage2, MenuStage3_NoCar);
        CurrentMenu = MenuStage3_NoCar;
    }

    public void ToStage3_CarOnly()
    {
        ChangeMenuElements(MenuStage2, MenuStage3_CarOnly);
        CurrentMenu = MenuStage3_CarOnly;
    }

    public void ExitBtnClick()
    {
        DialogWindowsController.ShowQuestionWindow("Выйти из программы?", 1);
    }

    public void Exit()
    {
        SaveData();
        DialogWindowsController.DialogQWindow.SetActive(false);
        Application.Quit();
    }

    public void BackToStage2()
    {
        ChangeMenuElements(CurrentMenu, MenuStage2);
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsyncronously(sceneIndex));
    }

    IEnumerator LoadAsyncronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        LoadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            progressText.text = (int)(progress * 100f) + "%";
            yield return null;
        }
    }

    void SaveData()
    {
        PlayerPrefs.SetInt(PrefName, DevicesListDropdown.value);////
        PlayerPrefs.Save();////
    }

    void Update()
    {
        if (DevicesListDropdown.options.Count == 0)
        {
            ToInfoPanel.interactable = false;
            ToStage2Button.interactable = false;
        }
        else
        {
            ToInfoPanel.interactable = true;
            ToStage2Button.interactable = true;
        }
    }
}