﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingAnim_Biker : MonoBehaviour
{
    private Animator anim;
    private float speed;
    public GameObject one_border;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (MenuController.NeededType == "Вело")
        {
            speed = GetComponent<Follower_Biker>().speed;
        }
        if (MenuController.NeededType == "Кинематика")
        {
            speed = GetComponent<Follower_Kinemat>().speed;
        }
        if (MenuController.NeededType == "Дорожка")
        {
            speed = GetComponent<Follower_Runner>().speed;
        }
        if (speed > 0)
        {
            anim.speed = speed / 10;
            anim.SetBool("isMoving", true);
        }
        else if (speed <= 0)
        {
            anim.SetBool("isMoving", false);
        }
    }
}