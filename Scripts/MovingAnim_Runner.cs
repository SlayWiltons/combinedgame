﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;

public class MovingAnim_Runner : MonoBehaviour
{
    private Animator anim;
    private float speed;
    public GameObject one_border;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (MenuController.NeededType == "Вело")
        {
            speed = GetComponent<Follower_Biker>().speed;
        }
        if (MenuController.NeededType == "Кинематика")
        {
            speed = GetComponent<Follower_Kinemat>().speed;
        }
        if (MenuController.NeededType == "Дорожка")
        {
            speed = GetComponent<Follower_Runner>().speed;
        }
        if (speed > 0 && speed < 6)
        {
            anim.speed = speed * 0.018f / 0.1f;
            anim.SetBool("isWalking", true);
            anim.SetBool("isRunning", false);
        }
        else if (speed == 0)
        {
            anim.SetBool("isWalking", false);
            anim.speed = 1;
        }
        if (speed >= 6 && speed <= 10)
        {
            anim.SetBool("isRunning", true);
            anim.speed = speed / 10;           
        }
    }
}