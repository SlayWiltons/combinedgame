﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RunnerSceneController : MonoBehaviour
{
    GameObject Character;
    public GameObject Biker;
    public GameObject Runner;
    public Button ButtonSound;

    public void Start()
    {
        if (MenuController.neededCharacter == "Biker")
        {
            Biker.SetActive(true);
            Runner.SetActive(false);
        }
        if (MenuController.neededCharacter == "Runner")
        {
            Runner.SetActive(true);
            Biker.SetActive(false);
        }
        //AudioListener.pause = false;
        AudioListener.volume = 1;
        ButtonSound.GetComponentInChildren<Text>().text = "Выкл. звук";
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void SoundControl()
    {
        if (AudioListener.volume == 1)
        {
            //AudioListener.pause = true;
            AudioListener.volume = 0;
            ButtonSound.GetComponentInChildren<Text>().text = "Вкл. звук";
        }
        else
        {
            //AudioListener.pause = false;
            AudioListener.volume = 1;
            ButtonSound.GetComponentInChildren<Text>().text = "Выкл. звук";
        }
    }
}