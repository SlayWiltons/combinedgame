﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    public GameObject Biker;
    public GameObject Runner;
    public GameObject Car;
    public Button ButtonSound;

    public void Start()
    {
        if (MenuController.neededCharacter == "Biker")
        {
            Biker.SetActive(true);
            Runner.SetActive(false);
            Car.SetActive(false);
        }
        if (MenuController.neededCharacter == "Runner")
        {
            Runner.SetActive(true);
            Biker.SetActive(false);
            Car.SetActive(false);
        }
        if (MenuController.neededCharacter == "Car")
        {
            Car.SetActive(true);
            Runner.SetActive(false);
            Biker.SetActive(false);
        }
        AudioListener.volume = 1;
        ButtonSound.GetComponentInChildren<Text>().text = "Выкл. звук";
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void SoundControl()
    {
        if (AudioListener.volume == 1)
        {
            AudioListener.volume = 0;
            ButtonSound.GetComponentInChildren<Text>().text = "Вкл. звук";
        }
        else
        {
            AudioListener.volume = 1;
            ButtonSound.GetComponentInChildren<Text>().text = "Выкл. звук";
        }
    }
}
