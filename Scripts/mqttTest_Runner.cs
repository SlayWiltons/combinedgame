﻿using UnityEngine;
using System.Collections;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System.IO;
using UnityEngine.UI;
using System;

public class mqttTest_Runner : MonoBehaviour
{
	public PostData json;
    public float qs;  //speed    
    public string path;
    public string path2; 
    public string strIP;
    public string strID;  
    public GameObject text_connect;
    private MqttClient client;

	void Start ()
    {
        StreamReader reader = new StreamReader(PrepController.strPathFileIP);
        strIP = reader.ReadLine();
        reader.Close();
        strID = StartController.neededID;

        // create client instance 
        Debug.Log("MQTT IP: " + strIP + " adress");
        Debug.Log("Device: " + strID + " checked");
        client = new MqttClient(IPAddress.Parse(strIP.Trim()),1883 , false , null ); //192.168.2.203    192.168.1.195
        Debug.Log("client- " + client);
        // register to message received 
        client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 		
		string clientId = Guid.NewGuid().ToString(); 
		client.Connect(clientId);		
		// subscribe to the topic "/home/temperature" with QoS 2 
		client.Subscribe(new string[] { strID + "/parameters" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE }); //androidclients/data     
	}

	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{
		String message = System.Text.Encoding.UTF8.GetString(e.Message);
		json = JsonUtility.FromJson<PostData> (message);

        if (json.qs >= 0 || json.qs <= 100)
        {
            qs = json.qs;
        }
        Debug.Log("Received speed: " + json.qs);
    } 

	public void sendMqtt(String val)
    {
		if (val == "stop_trademill")
        {
			Debug.Log ("Trademill stopped");
			client.Publish("trademill/settings", System.Text.Encoding.UTF8.GetBytes("{w: 0}"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		}
		if (val == "start_trademill")
        {
			Debug.Log ("Trademill Started");
			client.Publish("trademill/settings", System.Text.Encoding.UTF8.GetBytes("{w: 1}"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
		}
	}

	void Update ()
    {
        if (client.IsConnected == true)
        {
            text_connect.GetComponent<Text>().color = Color.green;
            text_connect.GetComponent<Text>().text = "Подключено";
        }
        if (client.IsConnected == false)
        {
            text_connect.GetComponent<Text>().color = Color.red;
            text_connect.GetComponent<Text>().text = "Не подключено";
        }
    }
}